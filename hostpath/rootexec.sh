export SERVICEACCOUNT=hostuser
#oc create serviceaccount $SERVICEACCOUNT
#oc adm policy add-scc-to-user anyuid -z $SERVICEACCOUNT
#oc adm policy add-scc-to-user hostmount-anyuid -z $SERVICEACCOUNT
oc adm policy add-scc-to-user privileged -z $SERVICEACCOUNT
oc patch dc/host --patch '{"spec":{"template":{"spec":{"serviceAccountName": '\"$SERVICEACCOUNT\"' }}}}'

oc set volume dc/host --add -m /host --name=v1 -t pvc --claim-name=task-pvc-volume

